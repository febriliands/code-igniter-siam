<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tambah Data KRS
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Data KRS</li>
      </ol>
    </section>

    <section class="content">
        <form method="post" action="<?php echo base_url('krs/tambah_krs_aksi') ?>">
        <div class="form-group">
            <label>Tahun Akademik</label>
            <input type="hidden" name="id_akad" class="form-control" value="<?php echo $id_akad; ?>">
            <input type="hidden" name="id_krs" class="form-control" value="<?php echo $id_krs; ?>">
            <input type="text" name="tahun_akademik" class="form-control" value="<?php echo $thn_akad_smt. '/' .$semester; ?>" readonly/>
        </div>

        <div class="form-group">
            <label>NIM Mahasiswa</label>
            <input type="text" name="nim" class="form-control" value="<?php echo $nim; ?>" readonly/>
        </div>

        <div class="form-group">
            <label>Mata Kuliah</label>
            <?php
                $query = $this->db->query('SELECT kode_mk, nama_mk FROM matakuliah');

                $dropdowns = $query->result();

                foreach($dropdowns as $dropdown){
                    $dropDownList[$dropdown->kode_mk] = $dropdown->nama_mk;
                }
                echo form_dropdown('kode_mk', $dropDownList, $kode_mk, 'class="form-control" id="kode_mk"');
            ?>
        </div>
            
        <button href="<?php echo base_url('krs/index') ?>" type="submit" class="btn btn-primary">Simpan</button>
        <?php echo anchor('krs/krs_aksi', '<div class="btn btn-danger"> Cancel </div>'); ?>

        </form>
    </section>
</div>