<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Mata Kuliah
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Mata Kuliah</li>
      </ol>
    </section>

    <section class="content">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Tambah Data Mata Kuliah</button>
        <a class="btn btn-danger" href="<?php echo base_url('matakuliah/cetak') ?>"> <i class="fa fa-print"></i> Cetak</a>

        <div class="navbar-form navbar-right">
            <?php echo form_open('mahasiswa/search') ?>
            <input type="text" name="keyword" class="form-control" placeholder="search">
            <button type="submit" class="btn btn-success">Cari</button>
            <?php echo form_close()?>
        </div>

        <table class="table">
            <tr>
                <th>NO</th>
                <th>KODE MATA KULIAH</th>
                <th>NAMA MATA KULIAH</th>
                <th>SKS</th>
                <th>PROGRAM STUDI</th>
                <th>EDIT</th>
                <th>HAPUS</th>
            </tr>

            <?php
                $no = 1;
                foreach ($matakuliah as $mk) : ?>
            
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $mk->kode_mk ?></td>
                <td><?php echo $mk->nama_mk ?></td>
                <td><?php echo $mk->sks ?></td>
                <td><?php echo $mk->prodi ?></td>
                <td><?php echo anchor('matakuliah/edit_mk/'.$mk->id_mk, '<div class="btn btn-info"><i class="fa fa-edit"></div>') ?></td>
                <td onclick="javascript: return confirm('Apakah anda yakin ingin menghapus data anda?')"><?php echo anchor('matakuliah/hapus/'.$mk->id_mk, '<div class="btn btn-danger"><i class="fa fa-trash"></div>') ?></td>
            </tr>

                <?php endforeach; ?>
        </table>
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">FORM TAMBAH DATA MATA KULIAH</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <?php echo form_open_multipart('matakuliah/tambah_data');?>
            <div class="form-group">
                <label>Kode Mata Kuliah</label>
                <input type="text" name="kode_mk" class="form-control" required>
            </div>   
            
            <div class="form-group">
                <label>Nama Mata Kuliah</label>
                <input type="text" name="nama_mk" class="form-control" required>
            </div> 

            <div class="form-group">
                <label>SKS</label>
                <input type="text" name="sks" class="form-control" required>
            </div> 

            <div class="form-group">
                <label for="prodi">Jurusan</label>
                <select class="form-control" name="prodi" class="form-control" required>
                    <option>SISTEM INFORMASI</option>    
                    <option>TEKNIK INFORMATIKA</option>    
                    <option>TEKNIK KOMPUTER</option>
                    <option>TEKNOLOGI INFORMASI</option>
                    <option>PENDIDIKAN TEKNOLOGI INFORMASI</option>    
            </select>
        </div> 

            <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>

        <?php echo form_close(); ?>
      </div>
   
    </div>
  </div>
</div>
</div>