<?php

class Matakuliah extends CI_Controller {

	function index()
	{
        $data['matakuliah'] = $this->model_matakuliah->tampilkan_data('matakuliah')->result();
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('matakuliah', $data);
		$this->load->view('templates/footer');
    }

    function tambah_data(){
        $kode_mk = $this->input->post('kode_mk');
        $nama_mk = $this->input->post('nama_mk');
        $sks = $this->input->post('sks');
        $semester = $this->input->post('semester');
        $prodi = $this->input->post('prodi');

        $data = array(
            'kode_mk' => $kode_mk,
            'nama_mk' => $nama_mk,
            'sks' => $sks,
            'semester' => $semester,
            'prodi' => $prodi
        );
        $this->model_matakuliah->input_data($data, 'matakuliah');
        redirect('matakuliah/index');
    }
     
    function hapus($id_mk){
        $where = array('id_mk' => $id_mk);
        $this->model_matakuliah->hapus_data($where, 'matakuliah');
        redirect('matakuliah/index');
    }

    function edit_mk($id_mk){
        $where = array('id_mk' => $id_mk);
        $data['matakuliah'] = $this->model_matakuliah->edit_data($where, 'matakuliah')->result();
        
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('edit_mk', $data);
		$this->load->view('templates/footer');
    }

    function update(){
        $id_mk = $this->input->post('id_mk');
        $kode_mk = $this->input->post('kode_mk');
        $nama_mk = $this->input->post('nama_mk');
        $sks = $this->input->post('sks');
        $semester = $this->input->post('semester');
        $prodi = $this->input->post('prodi');

        $data = array(
            'kode_mk' => $kode_mk,
            'nama_mk' => $nama_mk,
            'sks' => $sks,
            'semester' => $semester,
            'prodi' => $prodi
        );

        $where = array(
            'id_mk' => $id_mk
        );

        $this->model_matakuliah->update_data($where, $data, 'matakuliah');
        redirect('matakuliah/index');
    }

    function cetak(){
        $data['matakuliah'] = $this->model_matakuliah->tampilkan_data('matakuliah')->result();
        $this->load->view('cetak_mk', $data);
    }

}
?>