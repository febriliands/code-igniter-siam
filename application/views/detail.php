<div class="content-wrapper">
    <section class="content">
    <h4><strong>DATA MAHASISWA</strong></h4>    
    <table class="table">
            <tr>
                <th>NAMA MAHASISWA</th>
                <td><?php echo $detail->nama ?></td>
            </tr>
            <tr>
                <th>NIM</th>
                <td><?php echo $detail->nim ?></td>
            </tr>
            <tr>
                <th>TANGGAL LAHIR</th>
                <td><?php echo $detail->tgl_lahir ?></td>
            </tr>
            <tr>
                <th>JURUSAN</th>
                <td><?php echo $detail->jurusan ?></td>
            </tr>
            <tr>
                <th>ALAMAT ASAL</th>
                <td><?php echo $detail->alamat ?></td>
            </tr>
            <tr>
                <th>JENIS KELAMIN</th>
                <td><?php echo $detail->jeniskelamin ?></td>
            </tr>
            <tr>
                <th>AGAMA</th>
                <td><?php echo $detail->agama ?></td>
            </tr>
            <tr>
                <th>EMAIL</th>
                <td><?php echo $detail->email ?></td>
            </tr>
            <tr>
                <th>NOMOR HP</th>
                <td><?php echo $detail->no_hp ?></td>
            </tr>
            <tr>
                <td>
                    <img src="<?php echo base_url(); ?>assets/foto/<?php echo $detail->foto; ?> 
                    "width="100" height="110">               
                </td>
            </tr>
            
        </table>

        <a href="<?php echo base_url('mahasiswa/index'); ?>" class="btn btn-primary"> Kembali</a>
    </section>
</div>