<!DOCTYPE html>
<html>
<body>
    <table class="table" border="1px solid black" style="border-collapse: collapse;">
        <tr>
            <th>NO</th>
            <th>TAHUN AKADEMIK</th>
            <th>SEMESTER</th>
            <th>STATUS</th>
        </tr>

        <?php
        $no=1;
        foreach ($tahun_akademik as $ak): ?>

            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $ak->tahun_akademik ?></td>
                <td><?php echo $ak->semester ?></td>
                <td><?php echo $ak->status ?></td>
            </tr>

        <?php endforeach; ?>
    </table>

    <script type="text/javascript">
        window.print();
    </script>

</body>
</html>