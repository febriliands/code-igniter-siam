<?php

class Auth extends CI_Controller {

	public function login()
	{
		$this->load->view('login');
    }
    
    public function proses_login()
	{
        $this->form_validation->set_rules('username','username', 'required', ['required' => 'Username wajib diisi!']);
        $this->form_validation->set_rules('password','password', 'required', ['required' => 'Password wajib diisi!']);
        
        if($this->form_validation->run() == FALSE){
		$this->load->view('login');
        }
        else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user = $username;
            $pass = $password;

            $cek = $this->login_model->cek_login($user, $pass);
            print_r ($cek);

            if($cek->num_rows() > 0){
                foreach ($cek->result() as $ck){
                    $sess_data['username'] = $ck->username;
                    $sess_data['email'] = $ck->email;
                    $sess_data['level'] = $ck->level;
                    print_r ($sess_data);
                    $this->session->set_userdata($sess_data);
                }
                if($sess_data['level'] == 'admin'){
                    redirect('Admin/dashboard');
                }
                else{
                    $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Maaf, Username atau Password yang anda masukkan salah!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>');
                    redirect('Auth/login');
                }
                }else{
                    $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Maaf, Username atau Password yang anda masukkan salah!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>');
                    redirect('Auth/login');
            }
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        redirect('Auth/login');
    }
}
