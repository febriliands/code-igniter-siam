<?php

class Model_mahasiswa extends CI_Model{
    function tampilkan_data(){
        return $this->db->get('tabel_mahasiswa');
    }

    function input_data($data,$table){
        $this->db->insert($table,$data);
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function edit_data($where,$table){
        return $this->db->get_where($table,$where);
    }

    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function detail_data($id=NULL){
        $query = $this->db->get_where('tabel_mahasiswa', array ('id' => $id))->row();
        return $query;
    }

    function get_keyword($keyword){
        $this->db->select('*');
        $this->db->from('tabel_mahasiswa');
        $this->db->like('nama', $keyword);
        $this->db->or_like('nim', $keyword);
        $this->db->or_like('tgl_lahir', $keyword);
        $this->db->or_like('jurusan', $keyword);
        $this->db->or_like('alamat', $keyword);
        $this->db->or_like('jeniskelamin', $keyword);
        $this->db->or_like('agama', $keyword);
        $this->db->or_like('email', $keyword);
        $this->db->or_like('no_hp', $keyword);
        return $this->db->get()->result();
    }

    public $table = 'tabel_mahasiswa';
    public $id = 'nim';

    function get_by_id($id){
        $this->db->where($this->id,$id);
        return $this->db->get($this->table)->row();
    }
}
?>