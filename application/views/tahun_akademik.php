<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Tahun Akademik
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tahun Akademik</li>
      </ol>
    </section>

    <section class="content">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Tambah Tahun Akademik</button>
        <a class="btn btn-danger" href="<?php echo base_url('tahun_akademik/cetak') ?>"> <i class="fa fa-print"></i> Cetak</a>

        <div class="navbar-form navbar-right">
            <?php echo form_open('mahasiswa/search') ?>
            <input type="text" name="keyword" class="form-control" placeholder="search">
            <button type="submit" class="btn btn-success">Cari</button>
            <?php echo form_close()?>
        </div>

        <table class="table">
            <tr>
                <th>NO</th>
                <th>TAHUN AKADEMIK</th>
                <th>SEMESTER</th>
                <th>STATUS</th>
                <th>EDIT</th>
                <th>HAPUS</th>
            </tr>

            <?php
                $no = 1;
                foreach ($tahun_akademik as $ak) : ?>
            
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $ak->tahun_akademik ?></td>
                <td><?php echo $ak->semester ?></td>
                <td><?php echo $ak->status ?></td>
                <td><?php echo anchor('tahun_akademik/edit_akad/'.$ak->id_akad, '<div class="btn btn-info"><i class="fa fa-edit"></div>') ?></td>
                <td onclick="javascript: return confirm('Apakah anda yakin ingin menghapus data anda?')"><?php echo anchor('tahun_akademik/hapus/'.$ak->id_akad, '<div class="btn btn-danger"><i class="fa fa-trash"></div>') ?></td>
            </tr>

                <?php endforeach; ?>
        </table>
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">FORM TAMBAH DATA MATA KULIAH</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <?php echo form_open_multipart('tahun_akademik/tambah_data');?>
            <div class="form-group">
                <label>Tahun Akademik</label>
                <input type="text" name="tahun_akademik" class="form-control" required>
            </div>   
            
            <div class="form-group">
                <label>Semester</label>
                <select class="form-control" name="semester" class="form-control" required>
                    <option>Ganjil</option>    
                    <option>Genap</option>   
                </select>
            </div> 

            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status" class="form-control" required>
                    <option>Aktif</option>    
                    <option>Tidak Aktif</option>   
                </select>
            </div> 

            <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>

        <?php echo form_close(); ?>
      </div>
   
    </div>
  </div>
</div>
</div>