<div class="content-wrapper">
    <section class="content">
        <?php foreach($matakuliah as $mk) { ?>

        <form action="<?php echo base_url().'matakuliah/update'; ?>" method="post">
            <div class="form-group">
                <label>Kode Mata Kuliah</label>
                <input type="hidden" name="id_mk" class="form-control" value="<?php echo $mk->id_mk ?>">
                <input type="text" name="kode_mk" class="form-control" value="<?php echo $mk->kode_mk ?>">
            </div>   
            
            <div class="form-group">
                <label>Nama Mata Kuliah</label>
                <input type="text" name="nama_mk" class="form-control" value="<?php echo $mk->nama_mk ?>">
            </div> 

            <div class="form-group">
                <label>SKS</label>
                <input type="text" name="sks" class="form-control" value="<?php echo $mk->sks ?>">
            </div>

            <div class="form-group">
                <label for="prodi">Program Studi</label class="form-control" value="<?php echo $mk->prodi ?>">
                <select class="form-control" name="prodi">
                    <option>SISTEM INFORMASI</option>    
                    <option>TEKNIK INFORMATIKA</option>    
                    <option>TEKNIK KOMPUTER</option>
                    <option>TEKNOLOGI INFORMASI</option>
                    <option>PENDIDIKAN TEKNOLOGI INFORMASI</option>    
                </select>
            </div>

            <button type="close" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
        <?php } ?>
    </section>
</div>