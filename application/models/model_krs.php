<?php

class Model_krs extends CI_Model{

    public $table = 'krs';
    public $id = 'id_krs';

    function insert($data){
        $this->db->insert($this->table, $data);
    }

    function tampilkan_data(){
        return $this->db->get('krs');
    }

    function get_by_id($id){
        $this->db->where($this->id,$id);
        return $this->db->get($this->table)->row();
    }

    function input_data($data,$table){
        $this->db->insert($table,$data);
    }
    
    function baca_krs($nim, $thn_akad){
        $this->db->select('k.id_krs', 'k.kode_mk','m.nama_mk', 'm.sks');
        $this->db->from('krs as k');
        $this->db->where('k.nim', $nim);
        $this->db->where('k.id_akad', $thn_akad);
        $this->db->join('matakuliah as m','m.kode_mk = k.kode_mk');

        $krs = $this->db->get()->result();
        return $krs;
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
}
?>