<div class="content-wrapper">
    <section class="content">
        <?php foreach($mahasiswa as $mhs) { ?>

        <form action="<?php echo base_url().'mahasiswa/update'; ?>" method="post">
            <div class="form-group">
                <label>Nama Mahasiswa</label>
                <input type="hidden" name="id" class="form-control" value="<?php echo $mhs->id ?>">
                <input type="text" name="nama" class="form-control" value="<?php echo $mhs->nama ?>">
            </div>   
            
            <div class="form-group">
                <label>NIM</label>
                <input type="text" name="nim" class="form-control" value="<?php echo $mhs->nim ?>">
            </div> 

            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="date" name="tgl_lahir" class="form-control" value="<?php echo $mhs->tgl_lahir ?>">
            </div>

            <div class="form-group">
                <label for="jurusan">Jurusan</label class="form-control" value="<?php echo $mhs->jurusan ?>">
                <select class="form-control" name="jurusan">
                    <option>SISTEM INFORMASI</option>    
                    <option>TEKNIK INFORMATIKA</option>    
                    <option>TEKNIK KOMPUTER</option>
                    <option>TEKNOLOGI INFORMASI</option>
                    <option>PENDIDIKAN TEKNOLOGI INFORMASI</option>    
                </select>
            </div>

            <div class="form-group">
                <label>Alamat Asal</label>
                <input type="text" name="alamat" class="form-control" value="<?php echo $mhs->alamat ?>">
            </div>

            <div class="form-group">
            <label for="jeniskelamin">Jenis Kelamin</label class="form-control" value="<?php echo $mhs->jeniskelamin ?>">
                <select class="form-control" name="jeniskelamin">
                    <option>LAKI-LAKI</option>    
                    <option>PEREMPUAN</option>  
                </select>
            </div>

            <div class="form-group">
                <label for="agama">Agama</label class="form-control" value="<?php echo $mhs->agama ?>">
                <select class="form-control" name="agama">
                    <option>ISLAM</option>    
                    <option>KRISTEN</option>    
                    <option>KATOLIK</option>
                    <option>HINDU</option>
                    <option>BUDHA</option>
                    <option>KONG HU CU</option>    
                </select>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" value="<?php echo $mhs->email ?>">
            </div>

            <div class="form-group">
                <label>No HP</label>
                <input type="number" name="no_hp" class="form-control" value="<?php echo $mhs->no_hp ?>">
            </div>

            <button type="close" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
        <?php } ?>
    </section>
</div>