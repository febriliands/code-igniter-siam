<div class="content-wrapper">
    <section class="content-header">
      <h1>
        KRS
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">KRS</li>
      </ol>
    </section>

    <section class="content">
        <center class="mb-4">
            <legend class="mt-3"><strong>KARTU RENCANA STUDI</strong></legend>

            <table>
            <tr>
                <td><strong>NIM</strong></td>
                <td>&nbsp: <?php echo $nim ?></td>
            </tr>

            <tr>
                <td><strong>Nama</strong></td>
                <td>&nbsp: <?php echo $nama ?></td>
            </tr>

            <tr>
                <td><strong>Prodi</strong></td>
                <td>&nbsp: <?php echo $jurusan ?></td>
            </tr>

            <tr>
                <td><strong>Tahun Akademik (Semester)</strong></td>
                <td>&nbsp: <?php echo $tahun_akademik. '&nbsp;('.$semester.')' ?></td>
            </tr>

        </table>
        </center>

        <?php echo anchor('krs/tambah_krs/'.$nim.'/'.$id_akad,'<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Tambah Data KRS</button>')?>
        
        
        <table class="table table-bordered table-hover table-striped mt-4">
            <tr>
                <th>NO</th>
                <th>KODE MATA KULIAH</th>
                <th>NAMA MATA KULIAH</th>
                <th>SKS</th>
                <th>HAPUS</th>
            </tr>

            <?php
                $no = 1;
                $jumlahSks = 0;
                foreach ($krs_data as $krs) : ?>
            
            <tr>
                <td width="20px"><?php echo $no++; ?></td>
                <td><?php echo $krs->kode_mk; ?></td>
                <td><?php echo $krs->nama_mk; ?></td>
                <td><?php echo $krs->sks; $jumlahSks+=$krs->sks; ?></td>
                <td onclick="javascript: return confirm('Apakah anda yakin ingin menghapus data anda?')"><?php echo anchor('krs/delete/'.$krs->id_krs, '<div class="btn btn-danger"><i class="fa fa-trash"></div>') ?></td>
            </tr>

                <?php endforeach; ?>

                <tr>
                <td colspan="3" align="right"><strong>Jumlah SKS</strong></td>
                <td colspan="3"><strong><?php echo $jumlahSks; ?></strong></td>
                </tr>

        </table>

    </section>
</div>