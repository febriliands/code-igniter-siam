<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Mahasiswa
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Mahasiswa</li>
      </ol>
    </section>

    <section class="content">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Tambah Data Mahasiswa</button>
        <a class="btn btn-danger" href="<?php echo base_url('mahasiswa/cetak') ?>"> <i class="fa fa-print"></i> Cetak</a>

        <table class="table">
            <tr>
                <th>NO</th>
                <th>NAMA MAHASISWA</th>
                <th>NIM</th>
                <th>TANGGAL LAHIR</th>
                <th>JURUSAN</th>
                <th>EDIT</th>
                <th>HAPUS</th>
            </tr>

            <?php
                $no = 1;
                foreach ($mahasiswa as $mhs) : ?>
            
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $mhs->nama ?></td>
                <td><?php echo $mhs->nim ?></td>
                <td><?php echo $mhs->tgl_lahir ?></td>
                <td><?php echo $mhs->jurusan ?></td>
                <td><?php echo anchor('mahasiswa/detail/'.$mhs->id, '<div class="btn btn-success btn-sm"><i class="fa fa-search-plus"></div>') ?></td>
                <td><?php echo anchor('mahasiswa/edit/'.$mhs->id, '<div class="btn btn-info"><i class="fa fa-edit"></div>') ?></td>
                <td onclick="javascript: return confirm('Apakah anda yakin ingin menghapus data anda?')"><?php echo anchor('mahasiswa/hapus/'.$mhs->id, '<div class="btn btn-danger"><i class="fa fa-trash"></div>') ?></td>
            </tr>

                <?php endforeach; ?>
        </table>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">FORM TAMBAH DATA MAHASISWA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <?php echo form_open_multipart('mahasiswa/tambah_data');?>
            <div class="form-group">
                <label>Nama Mahasiswa</label>
                <input type="text" name="nama" class="form-control" required>
            </div>   
            
            <div class="form-group">
                <label>NIM</label>
                <input type="text" name="nim" class="form-control" required>
            </div> 

            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="date" name="tgl_lahir" class="form-control" required>
            </div> 

            <div class="form-group">
                <label for="jurusan">Jurusan</label>
                <select class="form-control" name="jurusan" class="form-control" required>
                    <option>SISTEM INFORMASI</option>    
                    <option>TEKNIK INFORMATIKA</option>    
                    <option>TEKNIK KOMPUTER</option>
                    <option>TEKNOLOGI INFORMASI</option>
                    <option>PENDIDIKAN TEKNOLOGI INFORMASI</option>    
            </select>
        </div> 

            <div class="form-group">
                <label>Alamat Asal</label>
                <input type="text" name="alamat" class="form-control" required>
            </div>

            <div class="form-group">
            <label for="jeniskelamin">Jenis Kelamin</label>
                <select class="form-control" name="jeniskelamin" class="form-control" required>
                    <option>LAKI-LAKI</option>    
                    <option>PEREMPUAN</option>   
            </select>
            </div>

            <div class="form-group">
            <label for="agama">Agama</label>
                <select class="form-control" name="agama" class="form-control" required>
                    <option>ISLAM</option>    
                    <option>KRISTEN</option>    
                    <option>KATOLIK</option>
                    <option>HINDU</option>
                    <option>BUDHA</option>
                    <option>KONG HU CU</option>    
            </select>
            </div>

            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required>
            </div>

            <div class="form-group">
                <label>No HP</label>
                <input type="number" name="no_hp" class="form-control" required>
            </div>

            <div class="form-group">
                <label>Upload Foto</label>
                <input type="file" name="foto" class="form-control" required>
            </div>

            <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>

        <?php echo form_close(); ?>
      </div>
   
    </div>
  </div>
</div>
</div>