<?php

class Model_tahunakademik extends CI_Model{

    function tampilkan_data($table){
        return $this->db->get($table);
    }

    function input_data($data,$table){
        $this->db->insert($table,$data);
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function edit_data($where,$table){
        return $this->db->get_where($table,$where);
    }

    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public $table = 'tahun_akademik';
    public $id = 'id_akad';

    function get_by_id($id){
        $this->db->where($this->id,$id);
        return $this->db->get($this->table)->row();
    }
}
?>