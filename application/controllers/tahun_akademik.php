<?php

class Tahun_akademik extends CI_Controller {

	function index(){
        $data['tahun_akademik'] = $this->model_tahunakademik->tampilkan_data('tahun_akademik')->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('tahun_akademik', $data);
		$this->load->view('templates/footer');
    }
    
    function tambah_data(){
        $tahun_akademik = $this->input->post('tahun_akademik');
        $semester = $this->input->post('semester');
        $status = $this->input->post('status');

        $data = array(
            'tahun_akademik' => $tahun_akademik,
            'semester' => $semester,
            'status' => $status
        );
        $this->model_tahunakademik->input_data($data, 'tahun_akademik');
        redirect('tahun_akademik/index');
    }
     
    function hapus($id_akad){
        $where = array('id_akad' => $id_akad);
        $this->model_tahunakademik->hapus_data($where, 'tahun_akademik');
        redirect('tahun_akademik/index');
    }

    function edit_akad($id_akad){
        $where = array('id_akad' => $id_akad);
        $data['tahun_akademik'] = $this->model_tahunakademik->edit_data($where, 'tahun_akademik')->result();
        
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('edit_akad', $data);
		$this->load->view('templates/footer');
    }

    function update(){
        $id_akad = $this->input->post('id_akad');
        $tahun_akademik = $this->input->post('tahun_akademik');
        $semester = $this->input->post('semester');
        $status = $this->input->post('status');

        $data = array(
            'tahun_akademik' => $tahun_akademik,
            'semester' => $semester,
            'status' => $status
        );

        $where = array(
            'id_akad' => $id_akad
        );

        $this->model_tahunakademik->update_data($where, $data, 'tahun_akademik');
        redirect('tahun_akademik/index');
    }

    function cetak(){
        $data['tahun_akademik'] = $this->model_tahunakademik->tampilkan_data('tahun_akademik')->result();
        $this->load->view('cetak_akad', $data);
    }
}
?>