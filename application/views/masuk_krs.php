<div class="content-wrapper">
    <section class="content-header">
      <h1>
        KRS
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">KRS</li>
      </ol>
    </section>

    <section class="content">
        
        <div class="navbar-form navbar-right">
            <?php echo form_open('mahasiswa/search') ?>
            <input type="text" name="keyword" class="form-control" placeholder="search">
            <button type="submit" class="btn btn-success">Cari</button>
            <?php echo form_close()?>
        </div>

        <div>
        <?php echo $this->session->flashdata('pesan'); ?>
        </div>

    <form method="post" action="<?php echo base_url('krs/krs_aksi') ?>">

        <div class="form-group">
            <label>NIM MAHASISWA</label>
            <input type="text" name="nim" placeholder="Masukkan NIM Mahasiswa" class="form-control"><?php echo form_error('nim', '<div class="text-danger small ml-2">', '</div>') ?>
        </div>

        <div class="form-group">
            <label>Tahun Akademik / Semester</label>
            <?php
                $query = $this->db->query('SELECT id_akad, semester, CONCAT(tahun_akademik,"/")
                AS thn_semester
                FROM tahun_akademik');

                $dropdowns = $query->result();

                foreach($dropdowns as $dropdown){
                    if($dropdown->semester == 1){
                        $tampilSemester = "Ganjil";
                    } else{
                        $tampilSemester = "Genap";     
                    }
                    $dropDownList[$dropdown->id_akad] = $dropdown->thn_semester." ".$tampilSemester;
                }
                echo form_dropdown('id_akad', $dropDownList,'','class="form-control" id="id_akad"');
            ?>
        </div>
        <button type="submit" class="btn btn-success">Proses</button>
    </form>
    </section>
</div>