<?php

class Krs extends CI_Controller {

	function index(){
        $data = array(
            'nim' => set_value('nim'),
            'id_akad' => set_value('id_akad')
        );
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('masuk_krs', $data);
		$this->load->view('templates/footer');
    }
    
    function krs_aksi(){
		$this->_rulesKrs();
        
        if($this->form_validation->run() == FALSE){
            $this->index();
        } else{
            $nim = $this->input->post('nim', TRUE);
            $thn_akad = $this->input->post('id_akad', TRUE);
        }

        if($this->model_mahasiswa->get_by_id($nim) == null){
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mahasiswa Belum Terdaftar!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>');
        redirect('krs');
        } 

        $data = array (
            'nim' => $nim,
            'id_akad' => $thn_akad,
            'nama' => $this->model_mahasiswa->get_by_id($nim)->nama
        );

        $dataKrs = array (
            'krs_data' => $this->baca_krs($nim,$thn_akad),
            'nim' => $nim,
            'id_akad' => $thn_akad,
            'tahun_akademik' => $this->model_tahunakademik->get_by_id($thn_akad)->tahun_akademik,
            'semester' => $this->model_tahunakademik->get_by_id($thn_akad)->semester==1?'Ganjil':'Genap',
            'nama' => $this->model_mahasiswa->get_by_id($nim)->nama,
            'jurusan' => $this->model_mahasiswa->get_by_id($nim)->jurusan
        );

        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('krs_list', $dataKrs);
		$this->load->view('templates/footer');
    }

    function baca_krs($nim, $thn_akad){
        $this->db->select('*');
        $this->db->from('krs as k');
        $this->db->where('k.nim', $nim);
        $this->db->where('k.id_akad', $thn_akad);
        $this->db->join('matakuliah as m','m.kode_mk = k.kode_mk');

        $krs = $this->db->get()->result();
        return $krs;
    }

    function _rulesKrs(){
        $this->form_validation->set_rules('nim', 'nim','required');
        $this->form_validation->set_rules('id_akad', 'id_akad','required');
    }

    function tambah_krs($nim, $thn_akad){
        $data = array(
            'id_krs' => set_value('id_krs'),
            'id_akad' => $thn_akad,
            'thn_akad_smt' => $this->model_tahunakademik->get_by_id($thn_akad)->tahun_akademik,
            'semester' => $this->model_tahunakademik->get_by_id($thn_akad)->semester==1?'Ganjil':'Genap',
            'nim' => $nim,
            'kode_mk' => set_value('kode_mk')
        );

        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('krs_form', $data);
		$this->load->view('templates/footer');
    }

    function tambah_krs_aksi(){
        $this->_rules();

        if($this->form_validation-> run() == FALSE){
            $this->tambah_krs($this->input->post('nim', TRUE),
            $this->input->post('id_akad', TRUE));
        } else{
            $nim = $this->input->post('nim', TRUE);
            $id_akad = $this->input->post('id_akad', TRUE);
            $kode_mk = $this->input->post('kode_mk', TRUE);

            $data = array(
                'id_akad' => $id_akad,
                'nim' => $nim,
                'kode_mk' => $kode_mk
            );

            $this->model_krs->insert($data);
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data KRS Berhasil Dimasukkan!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>');
            redirect('krs/krs_aksi');
        }
    }

    function _rules(){
        $this->form_validation->set_rules('id_akad', 'id_akad','required');
        $this->form_validation->set_rules('nim', 'nim','required');
        $this->form_validation->set_rules('kode_mk', 'kode_mk','required');
    }

    function cetak(){
        $data['krs'] = $this->model_krs->tampilkan_data('krs')->result();
        $this->load->view('cetak_krs', $data);
    }

    function delete($id){
        $where = array('id_krs' => $id);
        $this->model_krs->hapus_data($where, 'krs');
        redirect('krs/index');
    }
}
?>