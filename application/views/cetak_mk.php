<!DOCTYPE html>
<html>
<body>
    <table class="table" border="1px solid black" style="border-collapse: collapse;">
        <tr>
            <th>NO</th>
            <th>KODE MATA KULIAH</th>
            <th>NAMA MATA KULIAH</th>
            <th>SKS</th>
            <th>PROGRAM STUDI</th>
        </tr>

        <?php
        $no=1;
        foreach ($matakuliah as $mk): ?>

            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $mk->kode_mk ?></td>
                <td><?php echo $mk->nama_mk ?></td>
                <td><?php echo $mk->sks ?></td>
                <td><?php echo $mk->prodi ?></td>
            </tr>

        <?php endforeach; ?>
    </table>

    <script type="text/javascript">
        window.print();
    </script>

</body>
</html>