<div class="content-wrapper">
    <section class="content">
        <?php foreach($tahun_akademik as $ak) { ?>

        <form action="<?php echo base_url().'tahun_akademik/update'; ?>" method="post">
            <div class="form-group">
                <label>Tahun Akademik</label>
                <input type="hidden" name="id_akad" class="form-control" value="<?php echo $ak->id_akad ?>">
                <input type="text" name="tahun_akademik" class="form-control" value="<?php echo $ak->tahun_akademik ?>">
            </div>   
            
            <div class="form-group">
                <label for="semester">Semester</label class="form-control" value="<?php echo $ak->semester ?>">
                <select class="form-control" name="semester">
                    <option>Ganjil</option>    
                    <option>Genap</option>       
                </select>
            </div> 

            <div class="form-group">
                <label for="status">Status</label class="form-control" value="<?php echo $ak->status ?>">
                <select class="form-control" name="status">
                    <option>Aktif</option>    
                    <option>Tidak Aktif</option>       
                </select>
            </div>

            <button type="close" class="btn btn-danger" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
        <?php } ?>
    </section>
</div>