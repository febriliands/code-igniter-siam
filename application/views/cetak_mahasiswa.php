<!DOCTYPE html>
<html>
<body>
    <table class="table" border="1px solid black" style="border-collapse: collapse;">
        <tr>
            <th>NO</th>
            <th>NAMA MAHASISWA</th>
            <th>NIM</th>
            <th>TANGGAL LAHIR</th>
            <th>JURUSAN</th>
            <th>ALAMAT ASAL</th>
            <th>JENIS KELAMIN</th>
            <th>AGAMA</th>
            <th>EMAIL</th>
            <th>NOMOR HP</th>
        </tr>

        <?php
        $no=1;
        foreach ($mahasiswa as $mhs): ?>

            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $mhs->nama ?></td>
                <td><?php echo $mhs->nim ?></td>
                <td><?php echo $mhs->tgl_lahir ?></td>
                <td><?php echo $mhs->jurusan ?></td>
                <td><?php echo $mhs->alamat ?></td>
                <td><?php echo $mhs->jeniskelamin ?></td>
                <td><?php echo $mhs->agama ?></td>
                <td><?php echo $mhs->email ?></td>
                <td><?php echo $mhs->no_hp ?></td>
            </tr>

        <?php endforeach; ?>
    </table>

    <script type="text/javascript">
        window.print();
    </script>

</body>
</html>