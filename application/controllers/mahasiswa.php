<?php

class Mahasiswa extends CI_Controller {

	function index()
	{
        $data['mahasiswa'] = $this->model_mahasiswa->tampilkan_data()->result();
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('mahasiswa', $data);
		$this->load->view('templates/footer');
    }
    
     function tambah_data(){
        $nama = $this->input->post('nama');
        $nim = $this->input->post('nim');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $jurusan = $this->input->post('jurusan');
        $alamat = $this->input->post('alamat');
        $jeniskelamin = $this->input->post('jeniskelamin');
        $agama = $this->input->post('agama');
        $email = $this->input->post('email');
        $no_hp = $this->input->post('no_hp');
        if ($foto=''){

        }

        else{
            $config['upload_path'] = './assets/foto';
            $config['allowed_types'] = 'jpg|jpeg|png';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('foto')){
                echo "Upload Foto Gagal"; die();
            } else{
                $foto=$this->upload->data('file_name');
            }
        }

        $data = array(
            'nama' => $nama,
            'nim' => $nim,
            'tgl_lahir' => $tgl_lahir,
            'jurusan' => $jurusan,
            'alamat' => $alamat,
            'jeniskelamin' => $jeniskelamin,
            'agama' => $agama,
            'email' => $email,
            'no_hp' => $no_hp,
            'foto' => $foto
        );

        $this->model_mahasiswa->input_data($data, 'tabel_mahasiswa');
        redirect('mahasiswa/index');
    }

    function hapus($id){
        $where = array('id' => $id);
        $this->model_mahasiswa->hapus_data($where, 'tabel_mahasiswa');
        redirect('mahasiswa/index');
    }

    function edit($id){
        $where = array('id' => $id);
        $data['mahasiswa'] = $this->model_mahasiswa->edit_data($where, 'tabel_mahasiswa')->result();
        
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('edit', $data);
		$this->load->view('templates/footer');
    }

    function update(){
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $nim = $this->input->post('nim');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $jurusan = $this->input->post('jurusan');
        $alamat = $this->input->post('alamat');
        $jeniskelamin = $this->input->post('jeniskelamin');
        $agama = $this->input->post('agama');
        $email = $this->input->post('email');
        $no_hp = $this->input->post('no_hp');

        $data = array(
            'nama' => $nama,
            'nim' => $nim,
            'tgl_lahir' => $tgl_lahir,
            'jurusan' => $jurusan,
            'alamat' => $alamat,
            'jeniskelamin' => $jeniskelamin,
            'agama' => $agama,
            'email' => $email,
            'no_hp' => $no_hp,
        );

        $where = array(
            'id' => $id
        );

        $this->model_mahasiswa->update_data($where, $data, 'tabel_mahasiswa');
        redirect('mahasiswa/index');
    }

    function detail($id){
        $this->load->model('model_mahasiswa');
        $detail = $this->model_mahasiswa->detail_data($id);
        $data['detail'] = $detail;
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('detail', $data);
		$this->load->view('templates/footer');
    }

    function cetak(){
        $data['mahasiswa'] = $this->model_mahasiswa->tampilkan_data('tabel_mahasiswa')->result();
        $this->load->view('cetak_mahasiswa', $data);
    }

    function search(){
        $keyword = $this->input->post('keyword');
        $data['mahasiswa'] = $this->model_mahasiswa->get_keyword('keyword');
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('mahasiswa', $data);
		$this->load->view('templates/footer');
    }
    
}
?>